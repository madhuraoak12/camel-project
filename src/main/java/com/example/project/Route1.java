package com.example.project;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class Route1 extends RouteBuilder {
	 	
	@Override
	public void configure() throws Exception {
		 restConfiguration()
		 	.component("servlet")
		 	.port(8080)
		 	.host("localhost");
         
		 rest()
         	.get("/hello")
         	.produces("text/plain")
         	.route()
         	.setBody(constant("Hello World"));
	}	
}

FROM openjdk:latest
COPY ./target/project-1.0.jar .
EXPOSE 9091
CMD java -jar project-1.0.jar